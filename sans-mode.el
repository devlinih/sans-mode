;;; sans-mode.el --- Make typing sound like sans Undertale... I'm sorry -*- lexical-binding: t; -*-

;; This program was inspired by excellent software such as nyan-mode and
;; selectric-mode

;;; The program:
(defconst +mode-directory+ (file-name-directory (or load-file-name buffer-file-name)))

(defconst +sans-sound+ (concat +mode-directory+ "sans.wav"))
(defconst +megalovania+ (concat +mode-directory+ "mus_zz_megalovania.ogg"))

;; Right now requires the Linux dependency of aplay, I should find OSX and
;; Windows equivalents

;; TODO, if mplayer is going to be used for megalovania have it use mplayer on
;; other systems that don't use ALSA
(defun sans-make-sound ()
  "Make sans sounds"
  (start-process "sans-undertale" nil "aplay" +sans-sound+))

;; Megalovania, note that I copied the way nyan-mode does this
(defvar megalovania-process nil)

(defun sans/bad-time ()
  "you're gonna have a bad time."
  (interactive)
  (unless megalovania-process
    (setq megalovania-process (start-process-shell-command "megalovania" "megalovania" (concat "mplayer " +megalovania+ " -loop 0")))))

(defun sans/stop-bad-time ()
  "GET DUNKED ON"
  (interactive)
  (when megalovania-process
    (delete-process megalovania-process)
    (setq megalovania-process nil)))


;;;###autoload
(define-minor-mode sans-mode
  "Make typing sound like sans Undertale..."
  :global t
  :lighter " sans"
  :keymap nil
  (if sans-mode
      (add-hook 'post-self-insert-hook 'sans-make-sound)
    (remove-hook 'post-self-insert-hook 'sans-make-sound))
  )

(provide 'sans-mode)

;;; sans-mode.el ends here
